cmake_minimum_required(VERSION 3.0)
project(raytracer)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")
set(CMAKE_CXX_FLAGS "-std=c++11 -Wall -Wextra -pedantic -fexceptions")

set(CMAKE_BUILD_TYPE Debug)

find_package(SDL2 REQUIRED)
find_package(OpenCL REQUIRED)
find_package(OpenGL)

add_definitions(-DDEBUG="1")

aux_source_directory(src SRCS)
aux_source_directory(src/cl SRCS)
aux_source_directory(src/shapes SRCS)

aux_source_directory(libs LIBS)
aux_source_directory(libs/GL LIBS)
aux_source_directory(libs/glm LIBS)

#Hack for KDevelop
set(SRCS ${SRCS}
    src/main.cpp)

include_directories(${SDL2_INCLUDE_DIR} ${OPENGL_INCLUDE_DIRS} ${OPENCL_INCLUDE_DIRS})

add_executable(raytracer ${SRCS} ${LIBS})

target_link_libraries(raytracer
                      ${SDL2_LIBRARY}
                      ${OPENCL_LIBRARIES}
		      ${OPENGL_LIBRARIES})
      
file(GLOB CL_FILES
  "${PROJECT_SOURCE_DIR}/src/cl/*.cl"
)      
      
file(COPY ${CL_FILES} DESTINATION "${CMAKE_BINARY_DIR}/")