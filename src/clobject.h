#ifndef CLOBJECT_H
#define CLOBJECT_H

#include <memory>

#include "cl/device.h"

namespace Raytracer
{

class CLObject
{
public:
    explicit CLObject(std::shared_ptr<OpenCL::Device> clDevice);
    virtual ~CLObject();

    void tryUpdate();
    
protected:
    void requestUpdate(bool immediately = false);
    void update();
    
    virtual void updateCLData() = 0;
    
    inline bool isDirty() const
    {
        return m_dirty;
    }
    
protected:
    std::shared_ptr<OpenCL::Device> m_clDevice;
    
private:
    bool m_dirty;
    
};

}

#endif // CLOBJECT_H
