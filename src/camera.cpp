#include "camera.h"

#include <stdexcept>
#include <iostream>
#include <cmath>

#include "app.h"
#include "scene.h"

#include "cl/device.h"
#include "cl/program.h"
#include "cl/task.h"

using namespace Raytracer;

#pragma pack()
typedef struct Cam
{
    Cam(Position p, Vec2 d, cl_int w, cl_int h, cl_int fov) :
        width(w), invWidth(1.0f/w), height(h), invHeight(1.0f/h), 
        angle((cl_float) tan(M_PI * 0.5f * fov / 180.f)), 
        aspectRatio(w/(cl_float)h)
    {
        pos.s[0] = p.x;
        pos.s[1] = p.y;
        pos.s[2] = p.z;
        
        dir.s[0] = d.x;
        dir.s[1] = d.y;
    }
    
	cl_float3 pos;
    cl_float2 dir;
	cl_int width;
	cl_float invWidth;
	cl_int height;
	cl_float invHeight;
	cl_float angle;
	cl_float aspectRatio;
	
} cl_cam;

Camera::Camera(std::shared_ptr<OpenCL::Device> clDevice, int width, int height,
               Position pos, Vec2 direction, int fov) :
    CLObject(clDevice),
    m_fov(fov),
    m_pos(pos),
    m_direction(direction),
    m_texture(nullptr),
    m_clCam(nullptr),
    m_surface(nullptr)
{
	m_clCam = m_clDevice->createMemory(CL_MEM_READ_ONLY, sizeof(cl_cam));

    m_program.reset<OpenCL::Program>(
        new OpenCL::Program(m_clDevice, "./main.cl")
    );

    m_task.reset<OpenCL::Task>(
        new OpenCL::Task(m_clDevice,
                       m_program,
                       "render")
    );

	setRenderRect(width, height);

    requestUpdate(true);
    
	m_task->setArgument(0, &m_texture, sizeof(cl_mem));
	m_task->setArgument(1, &m_clCam, sizeof(cl_mem));
}

Camera::~Camera()
{
    if(m_texture)
        m_clDevice->removeMemory(m_texture);
	
	if(m_clCam)
		m_clDevice->removeMemory(m_clCam);

    if(m_surface)
        SDL_FreeSurface(m_surface);
}

SDL_Surface* Camera::draw(const Scene *scene)
{
    Uint32 tStart = SDL_GetTicks();
  
    tryUpdate();

    cl_mem clSpheres = scene->clSpheres();
    cl_uint clSpheresCount = scene->getSpheresCount();
    cl_mem clLights = scene->clLights();
    cl_uint clLightsCount = scene->getLightsCount();

    m_task->setArgument(2, &clSpheres, sizeof(cl_mem));
    m_task->setArgument(3, &clSpheresCount, sizeof(cl_uint));
    m_task->setArgument(4, &clLights, sizeof(cl_mem));
    m_task->setArgument(5, &clLightsCount, sizeof(cl_uint));

    m_task->runParallel(m_globCount, m_locCount, nullptr, 2);

    m_clDevice->readMemory(m_texture, m_surface->pixels, m_textureSize);

    Uint32 tEnd = SDL_GetTicks();
    
    printf("Draw %.5f FPS\n", 1000.0f / (tEnd - tStart));
    
    return m_surface;
}

void Camera::setWidth(int width)
{
    if(width > 0)
        m_width = width;
    else
        throw std::runtime_error("Invalid camera width");
}

void Camera::setHeight(int height)
{
    if(height > 0)
        m_height = height;
    else
        throw std::runtime_error("Invalid camera height");
}

void Camera::setRenderRect(int width, int height)
{
    static const int depth = 4;

    setWidth(width);
    setHeight(height);

    if(m_texture)
        m_clDevice->removeMemory(m_texture);

    m_textureSize = width*height*depth;
    m_texture = m_clDevice->createMemory(CL_MEM_READ_WRITE, m_textureSize);

    if(m_surface)
        SDL_FreeSurface(m_surface);

    m_surface = SDL_CreateRGBSurface(0, width, height, depth*8, 0, 0, 0, 0);
	
    m_globCount[0] = (size_t) width;
    m_globCount[1] = (size_t) height;
    
    m_locCount[0] = getOptimalThreadCount(width);
    m_locCount[1] = getOptimalThreadCount(height);
    
	requestUpdate();
}

void Camera::setFov(int fov)
{
	m_fov = fov;
	
	requestUpdate();
}

void Camera::setPosition(Position pos)
{
	if(m_pos.x == pos.x && m_pos.y == pos.y && m_pos.z == pos.z)
		return;
	
	m_pos = pos;
	
	requestUpdate();
}

void Camera::setDirection(Vec2 dir)
{
    if(m_direction.x == dir.x && m_direction.y == dir.y)
        return;
    
    m_direction = dir;
    
    requestUpdate();
}

void Camera::updateCLData()
{
	cl_cam cam(m_pos, m_direction, m_width, m_height, m_fov);
	
	m_clDevice->writeMemory(m_clCam, &cam, sizeof(cl_cam));
}

//FIXME:: bleh write it again
size_t Camera::getOptimalThreadCount(int size) const
{
    for(int i = 10 ; i > 0 ; i--)
    {
        if(size % i == 0)
            return i;
    }
    return 1;
}


