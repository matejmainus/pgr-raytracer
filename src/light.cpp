#include "light.h"

using namespace Raytracer;

Light::Light(std::shared_ptr<OpenCL::Device> clDevice, 
             Position pos, float strength, Color color):
    CLObject(clDevice),
    m_pos(pos), m_strength(strength), m_color(color)
{

}

Light::~Light()
{

}

void Light::updateCLData()
{

}
