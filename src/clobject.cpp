#include "clobject.h"

using namespace Raytracer;

CLObject::CLObject(std::shared_ptr<OpenCL::Device> clDevice) :
    m_clDevice(clDevice)
{

}

CLObject::~CLObject()
{

}

void CLObject::requestUpdate(bool immediately)
{
    m_dirty = true;
    
    if(immediately)
        update();
}

void CLObject::update()
{
    updateCLData();
    
    m_dirty = false;
}

void CLObject::tryUpdate()
{
    if(isDirty())
        update();
}
