#ifndef CAMERA_H
#define CAMERA_H

#include <memory>
#include <SDL.h>

#include "cl/cl.h"

#include "types.h"

#include "clobject.h"

namespace OpenCL
{
    class Device;
    class Program;
    class Task;
}

namespace Raytracer
{

class Scene;

class Camera : public CLObject
{
public:
    explicit Camera(std::shared_ptr<OpenCL::Device> clDevice, int width, int height,
                    Position pos, Vec2 direction, int fov = 50);
    virtual ~Camera();

    SDL_Surface* draw(const Scene *scene);

    void setPosition(Position pos);
    inline Position position() const
    {
        return m_pos;
    }
   
    void setDirection(Vec2 direction);
    inline Vec2 direction() const
    {
        return m_direction;
    }
    
    void setRenderRect(int width, int height);
    void setWidth(int width);
    void setHeight(int height);
    
    inline int width() const
    {
        return m_width;
    }
    
    inline int height() const
    {
        return m_height;
    }
    
    void setFov(int fov);
    
    inline int fov() const
    {
        return m_fov;
    }

protected:
    virtual void updateCLData();

private:
    size_t getOptimalThreadCount(int size) const;
    
private:
    int m_width;
    int m_height;
    int m_fov;
    
    size_t m_globCount[2];
    size_t m_locCount[2];
    
    Position m_pos;
    Vec2 m_direction;
    
    std::shared_ptr<OpenCL::Program> m_program;
    std::shared_ptr<OpenCL::Task> m_task;

    cl_mem m_texture;
    cl_mem m_clCam;
    
    size_t m_textureSize;

    SDL_Surface *m_surface;  
};

}

#endif // RAYTRACER_H
