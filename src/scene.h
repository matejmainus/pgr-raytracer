#ifndef SCENE_H
#define SCENE_H

#include <memory>
#include <vector>

#include "cl/cl.h"
#include "clobject.h"

namespace OpenCL
{
    class Device;
}

namespace Raytracer
{

class Sphere;
class Shape;
class Light;

class Scene : public CLObject
{
public:
    explicit Scene(std::shared_ptr<OpenCL::Device> clDevice);
    virtual ~Scene();

    void addSphere(Sphere *sphere);
    
    void addLight(Light *light);
    
    inline cl_mem clSpheres() const {
        return m_clSpheres;
    }
    
    inline cl_mem clLights() const {
        return m_clLights;
    }

    inline cl_uint getSpheresCount() const {
        return (cl_uint) m_spheres.size();
    }

    inline cl_uint getLightsCount() const {
        return (cl_uint) m_lights.size();
    }
    
protected:
    virtual void updateCLData();
    
private:
    template <typename T, typename U>
    void copyToCL(const std::vector <T *> &vec, cl_mem *mem);
   
private:
    cl_mem m_clSpheres;
    
    cl_mem m_clLights;
    
    std::vector<Sphere *> m_spheres;
    
    std::vector<Light *> m_lights;
};

}

#endif // SCENE_H
