#ifndef APP_H
#define APP_H

#include <memory>
#include <SDL.h>

namespace OpenCL
{
    class Device;  
}

namespace Raytracer
{

class Scene;
class Camera;

class App
{
public:
    virtual ~App();

    static App& instance();

    void init();
    int exec();
    void quit();

    void cleanUp();

    std::shared_ptr<OpenCL::Device> openCLDevice();

    SDL_Renderer* renderer() const;

private:
    explicit App();

    void onKeyDown(SDL_KeyboardEvent key);
    
    void onMouseMoved(int x, int y);
    
private:
    SDL_Window *m_window;
    SDL_Renderer *m_renderer;

    std::shared_ptr<OpenCL::Device> m_clDevice;

    Camera *m_camera;
    Scene *m_scene;
};

}

#endif // APP_H
