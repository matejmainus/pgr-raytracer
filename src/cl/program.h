#ifndef OPENCLPROGRAM_H
#define OPENCLPROGRAM_H

#include <string>
#include <memory>

#include "cl.h"

namespace OpenCL
{

class Device;

class Program
{
public:
    explicit Program(std::shared_ptr<Device> device, const std::string &filepath);
    virtual ~Program();

    cl_program program() const;

private:
    std::string loadFile(const std::string &filepath) const;

private:
    std::shared_ptr<Device> m_device;
    cl_program m_program;
};

}

#endif // OPENCLPROGAM_H
