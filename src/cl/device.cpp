#include "device.h"

#include <string>
#include <iostream>
#include <stdexcept>

using namespace OpenCL;

Device::Device() :
    m_context(nullptr),
    m_deviceId(nullptr),
    m_cmdQueue(nullptr)
{
    cl_int ret;
    cl_uint num_devices;
    cl_uint num_platforms;

    cl_platform_id platform_id = nullptr;

    ret = clGetPlatformIDs(1, &platform_id, &num_platforms);
    ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &m_deviceId, &num_devices);
    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not find  device "+std::to_string(ret));

    m_context = clCreateContext(nullptr, 1, &m_deviceId, nullptr, nullptr, &ret);
    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not create  device context "+std::to_string(ret));

    size_t maxWorkGroups;
    clGetDeviceInfo(m_deviceId, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &maxWorkGroups, nullptr);

    cl_uint maxDims;
    clGetDeviceInfo(m_deviceId, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &maxDims, nullptr);

    size_t *maxWorkItems = new size_t[maxDims];
    clGetDeviceInfo(m_deviceId, CL_DEVICE_MAX_WORK_ITEM_SIZES, maxDims*sizeof(size_t), maxWorkItems, nullptr);

    std::cout << "WorkGroups: " << maxWorkGroups << std::endl;
    std::cout << "Max dimms: " << maxDims << std::endl;

    for(cl_uint i = 0 ; i < maxDims ; i++)
        std::cout << "Items[" << i << "]:" << maxWorkItems[i] << std::endl;

    if(maxDims > 0)
        delete maxWorkItems;

    m_cmdQueue = clCreateCommandQueue(m_context, m_deviceId, 0, &ret);
    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not create command queue "+std::to_string(ret));
}

Device::~Device()
{
    clReleaseCommandQueue(m_cmdQueue);
    clReleaseContext(m_context);
}

cl_context Device::context() const
{
    return m_context;
}

cl_device_id Device::deviceId() const
{
    return m_deviceId;
}

cl_command_queue Device::cmdQueue() const
{
    return m_cmdQueue;
}

cl_mem Device::createMemory(cl_mem_flags flags, size_t size, void *hostPtr)
{
    cl_int ret;
    cl_mem memobj = clCreateBuffer(m_context, flags, size, hostPtr, &ret);

    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not create memory "+std::to_string(ret));

    return memobj;
}

void Device::readMemory(cl_mem memory, void* data, size_t size)
{
    cl_int ret = clEnqueueReadBuffer(m_cmdQueue, memory, CL_TRUE, 0,
                                     size, data, 0, nullptr, nullptr);

    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not read memory "+std::to_string(ret));
}

void Device::writeMemory(cl_mem memory, void* data, size_t size)
{
    cl_int ret = clEnqueueWriteBuffer(m_cmdQueue, memory, CL_TRUE, 0,
                                      size, data, 0, nullptr, nullptr);

    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not write into memory "+std::to_string(ret));
}

void Device::removeMemory(cl_mem memory)
{
    clReleaseMemObject(memory);
}
