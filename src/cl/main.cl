#include "structs.cl"

#define MAX_RAY_DEPTH 2

uchar intersect(private cl_sphere sphere, private cl_ray *ray, private float *t)
{
    private float3 dir = sphere.center - ray->orig;
    private float tca = dot(dir, ray->dir);
    
    if (tca < 0) 
        return 0;
    
    private float d2 = dot(dir, dir) - pow(tca, 2);
    
    if (d2 > sphere.radius2)
        return 0;
    
    private float thc = sqrt(sphere.radius2 - d2);

    *t = tca - thc;
    
    if(*t < 0)
        *t = tca + thc;
    
    return 1;
}

/*
float maxx(float a, float b){
    return (a > b) ? a : b;
}

float mixx(float a, float b, float mix){
	return b*mix + a*(1-mix);
}*/

void trace(private cl_ray *ray, global cl_sphere *spheres, private uint spheresCount,
			 global cl_light *lights, private uint lightsCount, private uchar depth, private cl_trace *tr)
{
    private float t = 1e100f, t0 = 0;
	private cl_sphere shape;
	private uchar found = 0;

    for (uchar i = 0; i < spheresCount; ++i) {
        if (intersect(spheres[i], ray, &t0) == 1) {
            if (t0 < t) {
                t = t0;
                shape = spheres[i];
				found = 1;
            }
        }
    }
    
	if(found == 0)
    {
        tr->color[0] = 0.1f;
        tr->color[1] = 0.4f;
        tr->color[2] = 0.8f;
        
		tr->diffuse = 1;
		return ;
	}
    
	private float3 phit = ray->orig + ray->dir * t;
	private float3 nhit = phit - shape.center;
	nhit = normalize(nhit);
	float3 pix = {0.0f,0.0f,0.0f};
	float bias = 1e-4;
		
	if (dot(ray->dir, nhit) > 0.0f){
		nhit = -nhit;
	}

	private cl_ray rr;
	rr.orig = phit + nhit * bias;

    if((shape.reflection > 0.0f) && depth < MAX_RAY_DEPTH) {
		float facingratio = -dot(ray->dir, nhit);
        float fresneleffect = mix(pow(1.0f - facingratio, 3.0f), 1.0f, 0.3f);
        
        float3 refldir = ray->dir - nhit * 2.0f * dot(ray->dir, nhit);
        rr.dir = normalize(refldir);
        tr->rayReflection = rr;
		tr->sphReflection = shape.reflection;
        
		tr->diffuse = 0;
		tr->sphColor = shape.color;
		tr->fresneleffect = fresneleffect;
	}
	else {
		tr->diffuse = 1;
	}
	
	for(uchar i=0; i < lightsCount; ++i){
		private float3 lightDirection = lights[i].position - phit;
		private float transmission = 1;
		lightDirection = normalize(lightDirection);
			
		for (uchar j = 0; j < spheresCount; ++j) {
			rr.dir = lightDirection;
            if (intersect(spheres[j], &rr, &t0) == 1) {
                transmission = 0;
                break;
            }
        }

        pix += shape.color * transmission * max(0.0f, dot(nhit, lightDirection)) * lights[i].color;// / lightsCount;
	}
    
	tr->color = pix;
}

uchar4 toColor(float3 color)
{
	uchar4 pix;
	pix[0] = (uchar) min(color[2] * 255, 255.0f);
	pix[1] = (uchar) min(color[1] * 255, 255.0f);
	pix[2] = (uchar) min(color[0] * 255, 255.0f);
	pix[3] = 0x00;
    
	return pix;
}

float3 traceWhole(private cl_ray *ray, global cl_sphere *spheres, private uint spheresCount,
			 global cl_light *lights, private uint lightsCount)
{
    private char i = 0;
	private cl_trace trs[MAX_RAY_DEPTH+1], tr;
	
    trace(ray, spheres, spheresCount, lights, lightsCount, 0, &tr);
	trs[0] = tr;
	
    while(trs[i].diffuse == 0) {
		trace(&(trs[i].rayReflection), spheres, spheresCount, lights, lightsCount, i+1, &tr);
		trs[i+1] = tr;

		i++;
	}
    
	i--;

	while(i >= 0){
		if(trs[i].diffuse == 0){
			trs[i].color = (trs[i+1].color * trs[i].fresneleffect * trs[i].sphReflection) * trs[i].sphColor
							+ (1-trs[i].sphReflection)*trs[i].color;
		}

		i--;
	}
    
	return trs[0].color;
}

float3 rotateMe(float2 angles, float3 vec){
	vec = normalize(vec);
	            
    float c = cos(angles[0]), s = sin(angles[0]), t = 1 - cos(angles[0]);

	float3 result;
    /* original equation
	result[0] = vec[0]*(t*axis[0]*axis[0]+c) + vec[1]*(t*axis[0]*axis[1]-s*axis[2]) + vec[2]*(t*axis[0]*axis[2]+s*axis[1]);
    result[1] = vec[0]*(t*axis[0]*axis[1]+s*axis[2]) + vec[1]*(t*axis[1]*axis[1]+c) + vec[2]*(t*axis[1]*axis[2]-s*axis[0]);
    result[2] = vec[0]*(t*axis[0]*axis[2]-s*axis[1]) + vec[1]*(t*axis[1]*axis[2]+s*axis[0]) + vec[2]*(t*axis[2]*axis[2]+c);
    */
	result[0] = vec[0]*(c) + vec[2]*(s);
    result[1] = vec[1];
    result[2] = vec[0]*(-s) + vec[2]*(c);

	vec = result;

	float3 axis = {c,0,-s};
	
	c = cos(angles[1]); s = sin(angles[1]); t = 1 - cos(angles[1]);

	result[0] = vec[0]*(t*axis[0]*axis[0]+c) + vec[1]*(-s*axis[2]) + vec[2]*(t*axis[0]*axis[2]);
    result[1] = vec[0]*(s*axis[2]) + vec[1]*(c) + vec[2]*(-s*axis[0]);
    result[2] = vec[0]*(t*axis[0]*axis[2]) + vec[1]*(s*axis[0]) + vec[2]*(t*axis[2]*axis[2]+c);

	return result;
}

kernel void render(global uchar4* surface, global cl_camera *cam, 
                   global cl_sphere *spheres, uint spheresCount,
				   global cl_light *lights, uint lightsCount)
{
    private float xx = (2 * ((get_global_id(0) + 0.5) * cam->inv_width) - 1) * cam->angle * cam->aspect_ratio;
    private float yy = (1 - 2 * ((get_global_id(1) + 0.5) * cam->inv_height)) * cam->angle;
    private float3 raydir = {xx, yy, -1};

    private cl_ray ray;
    ray.orig = cam->pos;
    ray.dir = normalize(rotateMe(cam->dir,raydir));
    
    surface[get_global_id(1) * get_global_size(0)+get_global_id(0)] = toColor(traceWhole(&ray, spheres, spheresCount, lights, lightsCount));
}