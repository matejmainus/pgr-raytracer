#pragma pack()
typedef struct
{
    float3 orig;
    float3 dir;
} cl_ray;

#pragma pack()
typedef struct
{
    float3 pos;
    float2 dir;
    int width;
    float inv_width;
    int height;
    float inv_height;
    float angle;
    float aspect_ratio;
} cl_camera;

#pragma pack()
typedef struct 
{
    char type;
    float3 center;
    float3 color;
    float reflection;
    float transparency;
} cl_shape;

#pragma pack()
typedef struct
{
    float3 center;
    float3 color;
    float reflection;

    float radius;
    float radius2;
	char uselessVariableToExtentendTo6ParamsBecauseOfWhoKnowsWhy;
} cl_sphere;

#pragma pack()
typedef struct
{
    float3 position;
    float3 color;
} cl_light;

#pragma pack()
typedef struct
{
    cl_sphere *spheres;
    uint spheresCount;
    
} cl_scene;

typedef struct
{
	float3 sphColor;
	float3 color;
	cl_ray rayReflection;
	float fresneleffect;
	float sphReflection;
	uchar diffuse;
} cl_trace;