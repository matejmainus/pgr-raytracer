#include "task.h"

#include <stdexcept>

#include "device.h"
#include "program.h"

using namespace OpenCL;

Task::Task(std::shared_ptr<Device> device, std::shared_ptr<Program> program, const std::string &funName) :
    m_device(device),
    m_program(program)
{
    cl_int ret;

    m_kernel = clCreateKernel(m_program->program(), funName.c_str(), &ret);
    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not create kernel from program "+std::to_string(ret));
}

Task::~Task()
{
    clReleaseKernel(m_kernel);
}

void Task::setArgument(cl_uint pos, void* data, size_t size)
{
    cl_int ret = clSetKernelArg(m_kernel, pos, size, data);
    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not set kernel arg "+std::to_string(ret));
}

void Task::run() const
{
    cl_int ret = clEnqueueTask(m_device->cmdQueue(), m_kernel, 0, nullptr, nullptr);
    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not enqueue task "+std::to_string(ret));
}

void Task::runParallel(size_t *globalItemCount, size_t *localItemCount, size_t *globalOffset, cl_uint dimm)
{
    cl_int ret = clEnqueueNDRangeKernel(m_device->cmdQueue(), m_kernel, dimm, globalOffset,
                                        globalItemCount, localItemCount, 0, nullptr, nullptr);

    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could not enqueue parallel task "+std::to_string(ret));
}

