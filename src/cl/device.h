#ifndef OPENCLDEVICE_H
#define OPENCLDEVICE_H

#include <string>

#include "cl.h"

namespace OpenCL
{

class Program;

class Device
{
public:
    virtual ~Device();
    explicit Device();

    cl_context context() const;
    cl_device_id deviceId() const;
    cl_command_queue cmdQueue() const;

    cl_mem createMemory(cl_mem_flags flags, size_t size, void *hostPtr = nullptr);

    void readMemory(cl_mem memory, void* data, size_t size);
    void writeMemory(cl_mem memory, void* data, size_t size);

    void removeMemory(cl_mem memory);

private:
    cl_context m_context;
    cl_device_id m_deviceId;
    cl_command_queue m_cmdQueue;
};

}

#endif // OPENCL_H
