#ifndef OPENCLTASK_H
#define OPENCLTASK_H

#include <memory>
#include <string>

#include "cl.h"

namespace OpenCL
{

class Device;
class Program;

class Task
{
public:
    explicit Task(std::shared_ptr<Device> device, std::shared_ptr<Program> program, const std::string &funName);
    virtual ~Task();

    void run() const;
    void runParallel(size_t *groupCount, size_t *itemCount, size_t *globallOffset, cl_uint dimm = 1);

    void setArgument(cl_uint pos, void *data, size_t size);

private:
    std::shared_ptr<Device> m_device;
    std::shared_ptr<Program> m_program;

    cl_kernel m_kernel;
};

}
#endif // OPENCLTASK_H
