#include "program.h"

#include <string>
#include <iostream>
#include <stdexcept>
#include <fstream>
#include <iterator>

#include "device.h"

using namespace OpenCL;

Program::Program(std::shared_ptr<Device> device,  const std::string &filepath) :
    m_device(device)
{
    std::string programSrc = loadFile(filepath);
    std::cout << "OpenCL program loaded: " << filepath << std::endl;

    const char *cProgramSrc = programSrc.c_str();
    size_t srcSize = programSrc.size();

    cl_int ret;
    cl_device_id deviceId = m_device->deviceId();

    m_program = clCreateProgramWithSource(m_device->context(), 1, (const char **) &cProgramSrc,
                                          (const size_t *) &srcSize, &ret);

    if(ret != CL_SUCCESS)
        throw new std::runtime_error("Could create program from source "+std::to_string(ret));

    ret = clBuildProgram(m_program, 1, &deviceId, nullptr, nullptr, nullptr);
    if(ret != CL_SUCCESS)
    {
        size_t log_size;
        clGetProgramBuildInfo(m_program, deviceId, CL_PROGRAM_BUILD_LOG, 0, nullptr, &log_size);

        char *log = (char *) malloc(log_size);

        clGetProgramBuildInfo(m_program, deviceId, CL_PROGRAM_BUILD_LOG, log_size, log, nullptr);

        std::cout << log;

        throw new std::runtime_error("Could not build program "+std::to_string(ret));
    }

    std::cout << "OpenCL program builded " << filepath << std::endl;
}

Program::~Program()
{
    clReleaseProgram(m_program);
}

std::string Program::loadFile(const std::string& filepath) const
{
    std::ifstream stream(filepath);
    if(stream.fail())
        throw std::runtime_error("Can't open \'" + filepath + "\'");

    return std::string(std::istream_iterator<char>(stream >> std::noskipws), std::istream_iterator<char>());
}

cl_program Program::program() const
{
    return m_program;
}
