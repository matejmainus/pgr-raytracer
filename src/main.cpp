#include "app.h"

#include <iostream>
#include <stdexcept>
#include <exception>

int main(int argc, char **argv) {

    try
    {
        Raytracer::App &app = Raytracer::App::instance();

        return app.exec();
    }
    catch (const std::exception & e)
    {
        std::cout << "Error: " << e.what() << std::endl;

        return -1;
    }

}
