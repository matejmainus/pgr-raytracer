#ifndef LIGHT_H
#define LIGHT_H


#include "types.h"
#include "clobject.h"

namespace Raytracer
{

#pragma pack()
typedef struct CLLight
{
    CLLight(Position pos, Color col)
    {
        position.s[0] = pos.x;
        position.s[1] = pos.y;
        position.s[2] = pos.z;
        color.s[0] = col.r;
        color.s[1] = col.g;
        color.s[2] = col.b;
    }
    
    cl_float3 position;
    cl_float3 color;
    
} cl_light;
    
class Light : CLObject
{
public:
    explicit Light(std::shared_ptr<OpenCL::Device> clDevice, 
                   Position pos, float strength, Color color);
    virtual ~Light();
    
    inline cl_light clObj() const
    {
        return cl_light(m_pos, m_color);
    }
    
protected:
    virtual void updateCLData();
    
private:
    Position m_pos;
    float m_strength;
    Color m_color;
};

}

#endif // LIGHT_H
