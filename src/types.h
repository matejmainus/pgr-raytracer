#ifndef TYPES_H
#define TYPES_H

namespace Raytracer
{
    typedef struct Vec2
    {
        Vec2(float px, float py) :
            x(px), y(py)
        {}
        
        float x;
        float y;
        
    } Vec2;
    
    typedef struct Position
    {
        Position(float px, float py, float pz) :
        x(px), y(py), z(pz)
        {}
        
        float x;
        float y;
        float z;
        
    } Position;

     
    typedef struct Color
    {
        Color(float cr, float cg, float cb) :
            r(cr), g(cg), b(cb)
        {}
        
        float r;
        float g;
        float b;
        
    } Color;

}

#endif // SHAPE_H
