#include "app.h"

#include "../libs/glm/glm.hpp"

#include <iostream>
#include <stdexcept>
#include <string>

#include "camera.h"
#include "scene.h"
#include "light.h"

#include "shapes/shape.h"
#include "shapes/sphere.h"

#include "cl/device.h"
#include "cl/program.h"


using namespace Raytracer;

App::App() :
    m_window(nullptr),
    m_renderer(nullptr),
    m_clDevice(new OpenCL::Device())
{
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS) != 0)
    {
        throw std::runtime_error("Can not init SDL2 library context "+std::string(SDL_GetError()));
    }

    m_scene = new Scene(m_clDevice);
    m_scene->addSphere(
        new Sphere(m_clDevice, Position(0, -10004, -20), 10000, Color(0.15, 0.2, 0.15), 0.3)
    );
    m_scene->addSphere(
        new Sphere(m_clDevice, Position(0, 0, -20), 4, Color(1.00, 0.32, 0.36), 0.8)
    );
    m_scene->addSphere(
        new Sphere(m_clDevice, Position(5, -1, -15), 2, Color(0.90, 0.76, 0.46), 0.5)
    );
    m_scene->addSphere(
        new Sphere(m_clDevice, Position(5, 0, -25), 3, Color(0.65, 0.77, 0.97), 0.5)
    );
    m_scene->addSphere(
        new Sphere(m_clDevice, Position(-5.5, 0, -15), 3, Color(0.90, 0.90, 0.90), 1)
    );
    m_scene->addSphere(
        new Sphere(m_clDevice, Position(-5, 2, -25), 4, Color(0.90, 0.90, 0.90), 1)
    );
    m_scene->addSphere(
        new Sphere(m_clDevice, Position(2, 5, -25), 3, Color(0.30, 0.20, 0.90), 0.3)
    );
    m_scene->addSphere(
        new Sphere(m_clDevice, Position(7, 4, -8), 3, Color(0.50, 0.50, 0.90), 0.5)
    );
    m_scene->addLight(
        new Light(m_clDevice, Position(0, 20, -30), 1.0f, Color(1,1,1))
    );
    m_scene->addLight(
        new Light(m_clDevice, Position(0, 7, 0), 1.0f, Color(1, 1, 1))
    );
    
    m_camera = new Camera(m_clDevice, 800, 600, Position(0,0,0), Vec2(0,0));

    init();
}

App::~App()
{
    cleanUp();

    if(m_scene)
    {
        delete m_scene;
        m_scene = nullptr;
    }

    if(m_camera)
    {
        delete m_camera;
        m_camera = nullptr;
    }

    SDL_Quit();
}

App& App::instance()
{
    static App instance;

    return instance;
}

void App::init()
{
    m_window = SDL_CreateWindow("Raytracer",
                                SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                m_camera->width(), m_camera->height(),
                                SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (!m_window)
    {
        cleanUp();

        throw std::runtime_error("Can not create window "+std::string(SDL_GetError()));
    }

    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!m_renderer)
    {
        cleanUp();

        throw std::runtime_error("Can not initialize renderer "+std::string(SDL_GetError()));
    }

    SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
    SDL_RenderClear(m_renderer);

    std::cout << "App init done" << std::endl;
    /*
    SDL_GLContext maincontext;
    maincontext = SDL_GL_CreateContext(m_window);
    
    SDL_GL_SetSwapInterval(1);
     */
}

void App::cleanUp()
{
    if(m_renderer)
    {
        SDL_DestroyRenderer(m_renderer);
        m_renderer = nullptr;
    }

    if(m_window)
    {
        SDL_DestroyWindow(m_window);
        m_window = nullptr;
    }

    std::cout << "App cleanUp done" << std::endl;
}

int App::exec()
{
    SDL_Event event;

    bool running = true;
    while(running)
    {
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
            case SDL_QUIT:
                running = false;
                break;

            case SDL_WINDOWEVENT_RESIZED:
                m_camera->setRenderRect(event.window.data1, event.window.data2);
                break;
                    
            case SDL_KEYDOWN:
                onKeyDown(event.key);
                break;
                    
            case SDL_MOUSEMOTION:
                onMouseMoved(event.motion.x, event.motion.y);
                break;
            }
        }

        m_scene->tryUpdate();
        
        SDL_Surface *surface = m_camera->draw(m_scene);

        SDL_Texture *texture = SDL_CreateTextureFromSurface(m_renderer, surface);

        SDL_RenderCopy(m_renderer, texture, nullptr, nullptr);

        SDL_DestroyTexture(texture);

        SDL_RenderPresent(m_renderer);
    }

    return 0;
}

void App::quit()
{
    SDL_Event quitEvt;
    quitEvt.type = SDL_QUIT;

    SDL_PushEvent(&quitEvt);
}

SDL_Renderer* App::renderer() const
{
    return m_renderer;
}

std::shared_ptr<OpenCL::Device> App::openCLDevice()
{
    return m_clDevice;
}

void App::onKeyDown(SDL_KeyboardEvent key)
{
    static const float V_MULT = 1.5f;
    static const float H_MULT = 1.5f;
    
    Position camPos = m_camera->position();
    Vec2 camRot = m_camera->direction();

    glm::vec3 pos(camPos.x, camPos.y, camPos.z);

    // rotation around y axis
    float c = cos(camRot.x), s = sin(camRot.x), t = 1 - cos(camRot.x);

    Position camDir(s,0,c);
    
    // rotation around xz axis
    glm::vec3 axis(c,0,-s);
    c = cos(camRot.y); s = sin(camRot.y); t = 1 - cos(camRot.y);

    glm::vec3 dir(camDir.x,camDir.y,camDir.z);
    dir.x = camDir.x*(t*axis.x*axis.x+c) + camDir.y*(-s*axis.z) + camDir.z*(t*axis.x*axis.z);
    dir.y = camDir.x*(s*axis.z) + camDir.y*(c) + camDir.z*(-s*axis.x);
    dir.z = camDir.x*(t*axis.x*axis.z) + camDir.y*(s*axis.x) + camDir.z*(t*axis.z*axis.z+c);

    //rotation around same axis by 90°
    glm::vec3 up;
    up.x = dir.x*(axis.x*axis.x) + dir.y*(-axis.z) + dir.z*(axis.x*axis.z);
    up.y = dir.x*(axis.z) + dir.z*(-axis.x);
    up.z = dir.x*(axis.x*axis.z) + dir.y*(axis.x) + dir.z*(axis.z*axis.z);

    switch(key.keysym.sym)
    {
        case SDLK_ESCAPE:
            quit();
            break;
            
        case SDLK_w:
            pos -= dir;
            break;
        
        case SDLK_s:
            pos += dir;
            break;
        
        case SDLK_a:
            pos += glm::normalize(glm::vec3(-dir.z, 0, dir.x));
            break;
            
        case SDLK_d:
            pos -= glm::normalize(glm::vec3(-dir.z, 0, dir.x));
            break;

        case SDLK_q:
            pos += up;
            break;
            
        case SDLK_e:
            pos -= up;
            break;
         
    };
    
    m_camera->setPosition(Position(pos.x, pos.y, pos.z));

}

void App::onMouseMoved(int x, int y)
{
    static int oldX = m_camera->width()/2, oldY = m_camera->height()/2;
   
    Vec2 rot((oldX - x )*M_PI/180, (oldY - y)*M_PI/180);
    
    //oldX = x;
    //oldY = y;
    
    m_camera->setDirection(rot);
}

