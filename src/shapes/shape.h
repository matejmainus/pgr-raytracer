#ifndef SHAPE_H
#define SHAPE_H

#include "../types.h"
#include "../clobject.h"

namespace Raytracer
{

class Shape : public CLObject
{
public:
    explicit Shape(std::shared_ptr<OpenCL::Device> clDevice);
    virtual ~Shape();
};

}

#endif // SHAPE_H
