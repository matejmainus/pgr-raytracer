#include "sphere.h"

#include <iostream>

using namespace Raytracer;

Sphere::Sphere(std::shared_ptr<OpenCL::Device> clDevice, 
               Position pos, float radius, Color color, float reflection) :
    Shape(clDevice),
    m_pos(pos),
    m_radius(radius),
    m_color(color),
    m_reflection(reflection)
{
    requestUpdate(true);
}

Sphere::~Sphere()
{
}

void Sphere::updateCLData()
{
}


