#ifndef SPHERE_H
#define SPHERE_H

#include "../types.h"
#include "shape.h"

namespace Raytracer
{

#pragma pack()
typedef struct CLSphere
{
    CLSphere(Position pos, Color col, float r, float reflection) :
        reflection(reflection),
        radius(r), radius2(r*r)
    {
        position.s[0] = pos.x;
        position.s[1] = pos.y;
        position.s[2] = pos.z;
            
        color.s[0] = col.r;
        color.s[1] = col.g;
        color.s[2] = col.b;
    }
    
    cl_float3 position;
    cl_float3 color;
    cl_float reflection;
    
    cl_float radius;
    cl_float radius2;
    cl_char uselessVariableToExtentendTo6ParamsBecauseOfWhoKnowsWhy;
    
} cl_sphere;
    
class Sphere : public Shape
{
public:
    explicit Sphere(std::shared_ptr<OpenCL::Device> clDevice, 
                    Position pos, float radius, Color color, float reflection);
    virtual ~Sphere();
    
    inline cl_sphere clObj() const
    {
        return cl_sphere(m_pos, m_color, m_radius, m_reflection);
    }
    
protected:
    virtual void updateCLData();
    
private:
    Position m_pos;
    float m_radius;
    Color m_color;
    float m_reflection;
};

}

#endif // SPHERE_H
