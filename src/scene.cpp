#include "scene.h"

#include <iostream>

#include "app.h"
#include "light.h"

#include "cl/device.h"
#include "shapes/shape.h"
#include "shapes/sphere.h"

using namespace Raytracer;

Scene::Scene(std::shared_ptr<OpenCL::Device> clDevice):
    CLObject(clDevice),
    m_clSpheres(nullptr),
    m_clLights(nullptr)
{
}

Scene::~Scene()
{
    if(m_clSpheres)
        m_clDevice->removeMemory(m_clSpheres);
    
    if(m_clLights)
        m_clDevice->removeMemory(m_clLights);
}

void Scene::addLight(Raytracer::Light* light)
{
    m_lights.push_back(light);
    
    requestUpdate();
}

void Scene::addSphere(Sphere* sphere)
{
    m_spheres.push_back(sphere);
    
    requestUpdate();
}

void Scene::updateCLData()
{
    if(m_spheres.size() > 0)
    {
        copyToCL<Sphere, cl_sphere>(m_spheres, &m_clSpheres);
    }

    
    if(m_lights.size() > 0)
    {
        copyToCL<Light, cl_light>(m_lights, &m_clLights);
    }

    //m_clDevice->writeMemory(m_clLight, &clL, sizeof(cl_light));
   
}

template <typename T, typename U>
void Scene::copyToCL(const std::vector <T *> &vec, cl_mem *mem)
{
    if(*mem != nullptr)
        m_clDevice->removeMemory(*mem);
    
    U *clItems = (U *) malloc(vec.size() * sizeof(U));
    size_t i = 0;
    
    for(T *item: vec)
    {
        clItems[i] = item->clObj();
        i++;
    }
    
    *mem = m_clDevice->createMemory(CL_MEM_COPY_HOST_PTR, vec.size() * sizeof(U), clItems);
    
    delete clItems;
}